#!/bin/bash

PROGRAM_NAME=$0

while [ "$1" != "" ]; do
  if [ $1 -le 32767 -a $1 -ge 2 ]; then
    echo "$((1 + RANDOM % $1))"
    exit 0
  fi
  shift
done

echo "Usage: $PROGRAM_NAME 2-32767"
echo "Generates a random number between 1 and 32767 and outputs it to STDOUT"
