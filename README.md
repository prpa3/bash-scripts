# Bash scripts

A small collection of useful bash scripts

## internet_test.sh

Continually pings a specified ip address (8.8.8.8 by default) until a successful ping.

Usage: ./internet_test.sh [-i | --ip 8.8.8.8]

## random.sh

Prints a random number between 1-32767

Usage: ./random.sh 1000

**NOTE**: *Not* suitable for cryptographic uses as numbers are slightly biased