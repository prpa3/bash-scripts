#!/bin/bash

IP="8.8.8.8"
HELP=0
PROGRAM_NAME=$0

while [ "$1" != "" ]
do
  case $1 in
    -i | --ip )
      shift
      IP=$1
      ;;
    -h | --help )
      HELP=1
      ;;
  esac
  shift
done

if [ $HELP = 1 ]
then
  echo "Usage: $0 [-i | --ip 8.8.8.8]"
  echo "Pings the specified ip address (8.8.8.8 by default) until it succeeds , then plays a sound to notify the user"
  exit 0
fi

trap "exit" INT

printf "\n"

while :
do
  ping -c1 $IP > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
    paplay /usr/share/sounds/freedesktop/stereo/complete.oga
    exit 0
  else
    printf "\033[A"
    echo "Time running: $SECONDS seconds"
  fi
done

